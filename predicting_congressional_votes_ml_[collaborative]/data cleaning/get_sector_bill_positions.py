import pandas as pd
import numpy as np
import time
start_time = time.time()

sector_df = pd.read_csv("bill_positions_cleaning_copy.csv")
politician_df = pd.read_csv("votes_clean")

temp_list = list()
sector_df1 = sector_df.loc[sector_df['congress'] == 112]
politician_df1 = politician_df.loc[politician_df['congress'] == 112]

sector_bills = sector_df1.bill_id.unique()
politician_bills = politician_df1.bill_id.unique()

intersection =list(set(sector_bills) & set(politician_bills))
temp_list = intersection + temp_list

sector_df2 = sector_df.loc[sector_df['congress'] == 113]
politician_df2 = politician_df.loc[politician_df['congress'] == 113]

sector_bills = sector_df2.bill_id.unique()
politician_bills = politician_df2.bill_id.unique()

intersection =list(set(sector_bills) & set(politician_bills))
bill_list = intersection + temp_list

sectors = sector_df.organization_catcode.unique()
new_df = pd.DataFrame()
for bill in bill_list:
    bill_rows = sector_df.loc[sector_df['bill_id'] == bill]
    for sector in sectors:
        columns = ["congress","bill_prefix","bill_number","bill_id","bill_topic","sector_code","sector_position"]
        temp_df = pd.DataFrame(index=[0], columns=columns)
        sector_rows = bill_rows.loc[sector_df['organization_catcode'] == sector]
        opinion = sector_rows['organization_position'].value_counts()
        if not opinion.empty:
            s_o = opinion.index[0]
            temp_df["sector_code"] = sector
            temp_df["sector_position"] = s_o
            temp_df["congress"] = bill_rows.iloc[0]["congress"]
            temp_df["bill_prefix"] = bill_rows.iloc[0]["bill_prefix"]
            temp_df["bill_number"] = bill_rows.iloc[0]["bill_number"]
            temp_df["bill_id"] = bill_rows.iloc[0]["bill_id"]
            temp_df["bill_topic"] = bill_rows.iloc[0]["bill_topic"]
            new_df = pd.concat([new_df, temp_df])
new_df.to_csv("sector_positions.csv")
print("--- %s seconds ---" % (time.time() - start_time))
