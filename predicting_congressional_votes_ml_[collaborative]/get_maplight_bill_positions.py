import os
import requests
import json
from datetime import datetime, timedelta
import pandas as pd
import numpy as np

full_df = pd.DataFrame()
for k in [112, 113]:
    url = 'http://classic.maplight.org/services_open_api/map.bill_list_v1.json?apikey=example&jurisdiction=us&session=' + str(k) + '&include_organizations=1&has_organizations=0'
    response = requests.get(url)
    content = response.text
    contentj = response.json() # "The type of the return value of .json() is a dictionary, so you can access values in the object by key."
    df = pd.json_normalize(contentj["bills"])

    for i in range(len(df["organizations"])): #iterates through bills
        organization_list = df.iloc[i]["organizations"] #all organization for one bill
        for j in range(len(organization_list)):
            if organization_list:
                organization_df = pd.json_normalize(organization_list[j]) #one organization's data
            else:
                organization_df = pd.DataFrame()
            organization_df["url"] = df.iloc[i]["url"] #get all of the data about the bill from the bill df
            organization_df["jurisdiction"] = df.iloc[i]["jurisdiction"]
            organization_df["session"] = df.iloc[i]["session"]
            organization_df["prefix"] = df.iloc[i]["prefix"]
            organization_df["number"] = df.iloc[i]["number"]
            organization_df["measure"] = df.iloc[i]["measure"]
            organization_df["topic"] = df.iloc[i]["topic"]
            organization_df["last_update"] = df.iloc[i]["last_update"]
            full_df = pd.concat([full_df, organization_df])
full_df.to_csv("bill_positions.csv")

# url = 'http://classic.maplight.org/services_open_api/map.bill_list_v1.json?apikey=example&jurisdiction=us&session=113&include_organizations=1&has_organizations=0'
# response = requests.get(url)
# content = response.text
# contentj = response.json() # "The type of the return value of .json() is a dictionary, so you can access values in the object by key."
# df = pd.json_normalize(contentj["bills"])
#
# for i in range(len(df["organizations"])):
#     organization_list = df.iloc[i]["organizations"]
#     if organization_list:
#         organization_df = pd.json_normalize(organization_list[0])
#     else:
#         organization_df = pd.DataFrame()
#     organization_df["url"] = df.iloc[i]["url"]
#     organization_df["jurisdiction"] = df.iloc[i]["jurisdiction"]
#     organization_df["session"] = df.iloc[i]["session"]
#     organization_df["prefix"] = df.iloc[i]["prefix"]
#     organization_df["number"] = df.iloc[i]["number"]
#     organization_df["measure"] = df.iloc[i]["measure"]
#     organization_df["topic"] = df.iloc[i]["topic"]
#     organization_df["last_update"] = df.iloc[i]["last_update"]
#     full_df = pd.concat([full_df, organization_df])

#full_df.to_csv("bill_positions.csv")