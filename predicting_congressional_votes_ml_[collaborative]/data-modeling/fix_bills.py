import numpy as np
import pandas as pd

data = pd.read_csv("final_data_only_opinion.csv")

bills = data.bill_id.unique()

for bill in bills:
    bill_df = data.loc[data["bill_id"] == bill]
    votes = bill_df["politician_vote_position"]
    votes_values = votes.value_counts()
    print(bill, votes_values)

bills = ['S.2363', 'S.47', 'S.815', 'S.622', 'S.2569', 'S.954', 'S.2244', 'S.2262', 'S.1238', 'S.3525']
new_df = data.loc[data["bill_id"].isin(bills)]
new_df.to_csv("final_data_only_opinion.csv")


