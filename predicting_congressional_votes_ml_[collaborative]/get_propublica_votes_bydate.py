import os
import requests
import json
from datetime import datetime, timedelta
import pandas as pd

headers = {'X-API-Key': os.environ['PROPUBLICA_PW']}

'''
For date range requests:
chamber: house or senate
start-date: YYYY-MM-DD format
end-date YYYY-MM-DD format
'''

startdate = datetime(2012, 11, 7)
enddate = datetime(2014, 11, 4)

# First, take in two dates
def sepe30dayrange(startdate, enddate):
    '''

    :param startdate:
    :param enddate:
    :return: returns a list from startdate to enddate inclusive, in 30 day intervals
    '''
    months_in_range = (enddate.year-startdate.year)*12 + enddate.month-startdate.month
    days_in_range = (enddate-startdate).days # need to cast this as an int instead of a timedelta object
    intv = timedelta(days=30)
    mid = []
    for i in range(months_in_range+1):
        next = (startdate + intv*i).strftime("%Y-%m-%d")
        mid.append(next)
    if days_in_range%30 != 0:
        # then just append the last date to mid
        mid.append(enddate.strftime("%Y-%m-%d"))
    return mid

# Hmm ok I need to make sure that I iterate through pairs of dates in such a way that I don't return any duplicate votes
'''
eg. if months_list = ['2012-11-07', '2012-12-07', '2013-01-06', '2013-02-05', '2013-03-07', '2013-04-06', '2013-05-06', '2013-06-05', '2013-07-05', '2013-08-04', '2013-09-03', '2013-10-03', '2013-11-02', '2013-12-02', '2014-01-01', '2014-01-31', '2014-03-02', '2014-04-01', '2014-05-01', '2014-05-31', '2014-06-30', '2014-07-30', '2014-08-29', '2014-09-28', '2014-10-28', '2014-11-04']
pair | start        | end
1    | '2012-11-07' | '2012-12-07'
2    | '2012-12-08' | '2013-01-06'
3    | '2013-01-07' | '2013-02-05'
'''
# startdate = '2012-11-07'
# enddate = '2012-12-07'
mid = sepe30dayrange(startdate, enddate)

# #GETTING VOTES FOR SENATE
# chamber = 'senate'
# first = -2
# second = -1
# full_df = pd.DataFrame()
# for i in range((int)(len(mid)/2)):
#     first = first + 2
#     second = second + 2
#     #https: // api.propublica.org / congress / v1 / bills / search.json?query = {query}
#     url = 'https://api.propublica.org/congress/v1/' + chamber + '/votes/' + mid[first] + '/' + mid[second] + '.json'
#     response = requests.get(url, headers=headers)
#     content = response.text
#     contentj = response.json() # "The type of the return value of .json() is a dictionary, so you can access values in the object by key."
#     df = pd.json_normalize(contentj["results"]["votes"])
#     full_df = pd.concat([full_df, df])
# full_df.to_csv('vote_senate_results.csv')
#
#GETTING VOTES FOR HOUSE
chamber = 'senate'
first = -2
second = -1
full_df = pd.DataFrame()
for i in range((int)(len(mid)/2)):
    first = first + 2
    second = second + 2
    url = 'https://api.propublica.org/congress/v1/' + chamber + '/votes/' + mid[first] + '/' + mid[second] + '.json'
    response = requests.get(url, headers=headers)
    content = response.text
    contentj = response.json() # "The type of the return value of .json() is a dictionary, so you can access values in the object by key."
    df = pd.json_normalize(contentj["results"]["votes"])
    full_df = pd.concat([full_df, df]) #all house bills that have been voted on between 2012 and 2014

completely_full_df = pd.DataFrame()
#for i in range(len(full_df["roll_call"])):
for i in range(len(full_df)):
    url = 'https://api.propublica.org/congress/v1/' + str(full_df.iloc[i]["congress"]) + "/" + chamber +"/sessions/" + str(full_df.iloc[i]["session"]) + "/votes/" + str(full_df.iloc[i]["roll_call"]) + ".json"
    response = requests.get(url, headers=headers)
    content = response.text
    contentj = response.json()  # "The type of the return value of .json() is a dictionary, so you can access values in the object by key."
    df = pd.json_normalize(contentj["results"]["votes"]["vote"]["positions"]) #all positions on a bill broken down by politician

    new_df = pd.DataFrame(columns=full_df.columns)
    temp_df = pd.DataFrame(columns=full_df.columns)
    new_df = pd.concat([new_df, df], axis = 1)
    for j in temp_df.columns:
        new_df[j] = full_df.iloc[i][j]
    completely_full_df = pd.concat([completely_full_df, new_df])

completely_full_df.to_csv('vote_senate_results.csv')

# chamber = 'senate'
# first = -2
# second = -1
# first = first + 2
# second = second + 2
# url = 'https://api.propublica.org/congress/v1/112/' + chamber + '/bills/passed.json'
# response = requests.get(url, headers=headers)
# content = response.text
# contentj = response.json() # "The type of the return value of .json() is a dictionary, so you can access values in the object by key."
# print(contentj)
# content_result = contentj["results"]
# print(content_result[0]["bills"])
# df = pd.json_normalize(contentj["results"][0]["bills"])

# #SENATE BILLS
# full_df = pd.DataFrame()
# url = 'https://api.propublica.org/congress/v1/112/' + chamber + '/bills/passed.json'
# response = requests.get(url, headers=headers)
# content = response.text
# contentj = response.json() # "The type of the return value of .json() is a dictionary, so you can access values in the object by key."
# df = pd.json_normalize(contentj["results"][0]["bills"])
# full_df = pd.concat([full_df, df])
#
# url = 'https://api.propublica.org/congress/v1/113/' + chamber + '/bills/passed.json'
# response = requests.get(url, headers=headers)
# content = response.text
# contentj = response.json() # "The type of the return value of .json() is a dictionary, so you can access values in the object by key."
# df = pd.json_normalize(contentj["results"][0]["bills"])
# full_df = pd.concat([full_df, df])
#
# full_df.to_csv('bills_senate_results.csv')
#
# #HOUSE BILLS
# chamber = 'house'
# full_df = pd.DataFrame()
# url = 'https://api.propublica.org/congress/v1/112/' + chamber + '/bills/passed.json'
# response = requests.get(url, headers=headers)
# content = response.text
# contentj = response.json() # "The type of the return value of .json() is a dictionary, so you can access values in the object by key."
# df = pd.json_normalize(contentj["results"][0]["bills"])
# full_df = pd.concat([full_df, df])
#
# url = 'https://api.propublica.org/congress/v1/113/' + chamber + '/bills/passed.json'
# response = requests.get(url, headers=headers)
# content = response.text
# contentj = response.json() # "The type of the return value of .json() is a dictionary, so you can access values in the object by key."
# df = pd.json_normalize(contentj["results"][0]["bills"])
# full_df = pd.concat([full_df, df])
#
# full_df.to_csv('bills_house_results.csv')




