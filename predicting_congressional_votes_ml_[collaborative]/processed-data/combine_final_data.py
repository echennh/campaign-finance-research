import pandas as pd
import numpy as np

full_data = pd.read_csv("full_data.csv")
contributions = pd.read_csv("2014_contr_n_sectors_1.0.0.csv")

full_data_politicians = full_data.politician_name.unique()
contributions_politicians = contributions.RecipientCandidateNameNormalized.unique()

intersection =list(set(full_data_politicians) & set(contributions_politicians))
politicians = intersection

new_df = pd.DataFrame()
columns = ["politician_member_id","politician_name","politician_party","politician_state","politician_district","politician_vote_position","bill_id"
    ,"congress","bill_topic","sector_code","sector_position", "TransactionAmount"]

for bill in bills:
    temp_df = pd.DataFrame(index=[0], columns=columns)
    bill_rows_sector = sector_df.loc[sector_df['bill_id'] == bill]
    congress = bill_rows_sector.congress.unique()[0]
    congress_rows_politicians = politician_df.loc[politician_df['congress'] == congress]
    bill_rows = congress_rows_politicians.loc[congress_rows_politicians['bill_id'] == bill]

    politicians = bill_rows.politician_name.unique()
    for name in politicians:
        politician_row = full_data.loc[full_data['politician_name'] == name]
        for sector in bill_rows_sector.sector_code.unique():

            temp_df["politician_member_id"] = politician_row.iloc[0]["politician_member_id"]
            temp_df["politician_name"] = politician_row.iloc[0]["politician_name"]
            temp_df["politician_party"] = politician_row.iloc[0]["politician_party"]
            temp_df["politician_state"] = politician_row.iloc[0]["politician_state"]
            temp_df["politician_district"] = politician_row.iloc[0]["politician_district"]
            temp_df["politician_vote_position"] = politician_row.iloc[0]["politician_vote_position"]
            temp_df["bill_id"] = bill_rows.iloc[0]["bill_id"]
            temp_df["congress"] = bill_rows.iloc[0]["congress"]
            sector_row = bill_rows_sector.loc[bill_rows_sector['sector_code'] == sector]
            temp_df["bill_topic"] = sector_row.iloc[0]["bill_topic"]
            temp_df["sector_code"] = sector_row.iloc[0]["sector_code"]
            temp_df["sector_position"] = sector_row.iloc[0]["sector_position"]
            new_df = pd.concat([new_df, temp_df])
new_df.to_csv("full_data.csv")