import pandas as pd
import numpy as np

sector_df = pd.read_csv("sector_positions.csv")
politician_df = pd.read_csv("../data cleaning/votes_clean")
contributions = pd.read_csv("2014_contr_n_sectors_1.0.0.csv")

# get unique bills
twelve_sector = sector_df.loc[sector_df['congress'] == 112]
twelve_votes = politician_df.loc[politician_df['congress'] == 112]
twelve_bills_intersection = list(set(twelve_votes.bill_id.unique()) & set(twelve_sector.bill_id.unique()))

thir_sector = sector_df.loc[sector_df['congress'] == 113]
thir_votes = politician_df.loc[politician_df['congress'] == 113]
thir_bills_intersection = list(set(thir_sector.bill_id.unique()) & set(thir_votes.bill_id.unique()))

bills = thir_bills_intersection + twelve_bills_intersection

new_df = pd.DataFrame()
columns = ["politician_member_id","politician_name","politician_party","politician_state","politician_district",
           "politician_vote_position", "bill_id", "congress", "bill_topic", "sector_code","sector_position", "TransactionAmount"]

for bill in bills:

    # get congress from sector opinion dataset
    bill_rows_sector = sector_df.loc[sector_df['bill_id'] == bill]  # sectors that have an opinion on the bill
    congress = bill_rows_sector.congress.unique()[0]  # get congress of the bill
    # need politicians voting on the bill
    politicians_voting = politician_df.loc[(politician_df["bill_id"] == bill) & (politician_df["congress"] == congress)]
    politicians_intersection_for_bill = list(set(politicians_voting.politician_name.unique()) & set(contributions.RecipientCandidateNameNormalized.unique()))
    for name in politicians_intersection_for_bill: #for each politician that voted on the bill and recieved donations
        politician_row = politicians_voting.loc[politicians_voting["politician_name"] == name] #politician information for certain bill

        # for each politician, find which sectors contributed
        sectors_contributed_politician = (contributions.loc[contributions['RecipientCandidateNameNormalized'] == name]).organization_catcode.unique()
        #sector_intersection = list(set(sectors_contributed_politician) & set(bill_rows_sector.sector_code.unique()))

        sector_union = list(sectors_contributed_politician) + list(bill_rows_sector.sector_code.unique())

        for sector in sector_union: #for each sector that donated to the politician and has an opinion on the bill
            temp_df = pd.DataFrame(index=[0], columns=columns)
            sector_row = bill_rows_sector.loc[bill_rows_sector['sector_code'] == sector]
            temp_df["politician_member_id"] = politician_row.iloc[0]["politician_member_id"]
            temp_df["politician_name"] = politician_row.iloc[0]["politician_name"]
            temp_df["politician_party"] = politician_row.iloc[0]["politician_party"]
            temp_df["politician_state"] = politician_row.iloc[0]["politician_state"]
            temp_df["politician_district"] = politician_row.iloc[0]["politician_district"]
            temp_df["politician_vote_position"] = politician_row.iloc[0]["politician_vote_position"]
            temp_df["bill_id"] = politician_row.iloc[0]["bill_id"]
            temp_df["congress"] = politician_row.iloc[0]["congress"]
            temp_df["bill_topic"] = politician_row.iloc[0]["bill.title"]
            temp_df["sector_code"] = sector
            if sector in bill_rows_sector.sector_code.unique():
                temp_df["sector_position"] = sector_row.iloc[0]["sector_position"]
            else:
                temp_df["sector_position"] = None
            sector_contribution = contributions.loc[(contributions['organization_catcode'] == sector) & (contributions['RecipientCandidateNameNormalized'] == name)]
            if not sector_contribution.empty:
                temp_df["TransactionAmount"] = sector_contribution.iloc[0]["TransactionAmount"]
            else:
                temp_df["TransactionAmount"] = None
            sector_contribution = sector_contribution.iloc[0:0]
            new_df = pd.concat([new_df, temp_df])
new_df.to_csv("final_data_all.csv")




