import os


def load_postgres_pw():
    postgres_pw = os.environ.get('POSTGRES_PW')
    if postgres_pw is not None:
        print('POSTGRES_PW loaded successfully.')
    else:
        print('Failed to load POSTGRES_PW.')
    return postgres_pw