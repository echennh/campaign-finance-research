import pandas as pd
import os
import requests
os.getcwd()
# I need to make sure I'm in the right working directory
data = pd.read_csv("raw data/any_transaction_from_one_committee_to_another.txt", sep="|", header=None)
"""I get the following error:
C:\Program Files\JetBrains\PyCharm 2019.2.2\helpers\pydev\_pydev_bundle\pydev_console_types.py:35: DtypeWarning: Columns (10,11,12,15,16,18,19) have mixed types. Specify dtype option on import or set low_memory=False.
  self.more = self.interpreter.runsource(text, '<input>', symbol)"""
# data.columns = ["a", "b", "c", "etc."]

PRIVATE_API_KEY = os.environ.get("PRIVATE_API_KEY")
parameters = {"api_key":PRIVATE_API_KEY, "designation":'J'}
response = requests.get('https://api.open.fec.gov/v1/committees', params = parameters)
print(response.json())

