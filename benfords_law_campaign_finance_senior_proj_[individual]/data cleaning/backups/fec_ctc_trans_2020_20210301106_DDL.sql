/* This file is pretty much the raw data. I'm backing it up before data cleaning, dropping unnecessary columns and rows. */
-- public.fec_ctc_trans_2020 definition

-- Drop table

-- DROP TABLE public.fec_ctc_trans_2020;

CREATE TABLE public.fec_ctc_trans_2020 (
	cmte_id varchar(9) NULL,
	amndt_ind varchar(1) NULL,
	rpt_tp varchar(3) NULL,
	transaction_pgi varchar(5) NULL,
	image_num text NULL,
	transaction_tp varchar(3) NULL,
	entity_tp varchar(3) NULL,
	name varchar(200) NULL,
	city varchar(30) NULL,
	state varchar(2) NULL,
	zip_code varchar(9) NULL,
	employer varchar(38) NULL,
	occupation varchar(38) NULL,
	transaction_dt date NULL,
	transaction_amt numeric NULL,
	other_id varchar(9) NULL,
	tran_id varchar(32) NULL,
	file_num numeric NULL,
	memo_cd varchar(1) NULL,
	memo_text varchar(100) NULL,
	sub_id numeric NOT NULL,
	CONSTRAINT fec_ctc_trans_2020_pkey PRIMARY KEY (sub_id)
);