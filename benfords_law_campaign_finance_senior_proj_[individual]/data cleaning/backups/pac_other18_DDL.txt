-- public.pac_other18 definition

-- Drop table

-- DROP TABLE public.pac_other18;

CREATE TABLE public.pac_other18 (
	"cycle" bpchar(4) NOT NULL,
	fecrecno bpchar(19) NOT NULL,
	filerid bpchar(9) NOT NULL,
	donorcmte varchar(50) NULL,
	contriblendtrans varchar(50) NULL,
	city varchar(30) NULL,
	state bpchar(2) NULL,
	zip bpchar(5) NULL,
	fecoccemp varchar(38) NULL,
	primcode bpchar(5) NULL,
	"date" timestamp(0) NULL,
	amount float8 NULL,
	recipid bpchar(9) NULL,
	party bpchar(1) NULL,
	otherid bpchar(9) NULL,
	recipcode bpchar(2) NULL,
	recipprimcode bpchar(5) NULL,
	amend bpchar(1) NULL,
	report bpchar(3) NULL,
	pg bpchar(1) NULL,
	microfilm text NULL,
	"type" bpchar(3) NULL,
	realcode bpchar(5) NULL,
	"source" bpchar(5) NULL
);