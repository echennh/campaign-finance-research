import env_vars
import psycopg2
import sys
import pandas as pd

postgres_pw = env_vars.load_postgres_pw()


def connect():
    conn = None
    try:
        conn = psycopg2.connect(dbname="postgres", user="postgres", host="localhost", password=postgres_pw)
        # Create a cursor to perform database operations.
        cursor = conn.cursor()
        # Executing a SQL query
        cursor.execute("select version();")
        # Fetch result
        record = cursor.fetchone()
        print "You are connected to -", record, "\n"
    except (Exception, psycopg2.DatabaseError) as error:
        print "Error while connecting to the database.", error
        sys.exit(1)
    print "Connection successful"
    return conn

def postgresql_to_dataframe(conn, select_query, column_names):
    ''' Transform a SELECT query into a pandas dataframe '''
    cursor = conn.cursor()
    try:
        cursor.execute(select_query)
    except (Exception, psycopg2.DatabaseError) as error:
        print "Error: %s" % error
        cursor.close()
        return 1

    # Naturally we get a list of tuples
    tuples = cursor.fetchall()
    cursor.close()

    # We just need to turn it into a pandas dataframe
    df = pd.DataFrame(tuples, columns=column_names)
    return df

conn = connect()

column_names = ["cmte_id","amndt_ind","rpt_tp","transaction_pgi","image_num","transaction_tp","entity_tp","name","city","state", "zip_code", "employer", "occupation", "transaction_dt", "transaction_amt", "other_id", "tran_id", "file_num", "memo_cd", "memo_text", "sub_id"]
# Execute the "select *" query
df = postgresql_to_dataframe(conn, "select * from fec_ctc_trans_2020", column_names)
df.head()

# Close the connection
conn.close()

# Some code used from: https://naysan.ca/2020/05/31/postgresql-to-pandas/
