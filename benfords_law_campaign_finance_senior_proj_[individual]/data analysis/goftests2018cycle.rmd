---
title: "Benford's Distribution Goodness of Fit Tests for 2018 Election Cycle"
author: "Elisa Chen"
date: "4/21/2021"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(BenfordTests)
```


```{r}
getwd()
setwd("C:/Users/echen/Documents/Campaign Finance Research/campaign-finance-research/benfords_law_campaign_finance_senior_proj_[individual]/data cleaning")
cy2018 <- read.csv(file='FEC-itoth-2018cycle-ctc-inkind.csv', header=TRUE, sep="|")
head(cy2018)
attach(cy2018)
```

```{r}
chisq.benftest(TRANSACTION_AMT)
```

```{r}
edist.benftest(TRANSACTION_AMT, digits=1)
```


```{r}
ks.benftest(TRANSACTION_AMT, digits=1)

```

The null hypothesis of all these goodness of fit tests is that the data do follow Benford's distribution. For all of them, the p-value is very small, almost zero, which would mean that I should definitely reject the null hypothesis in favor of the alternative - that the 2018 election cycle data signficantly deviates from Benford's Law. 