# Campaign Finance Research
**Statement of Purpose:**
I want to learn how to execute a statistical research project from start to finish on campaign finance data, and algorithms for how to detect fraud. <br> <br>
**Problem statement:**
In this project, I will compare a Benford’s distribution model with in-kind donations to joint fundraising committees in the 2020 federal election as an initial analysis on campaign finance data for anomalies, which could be fraud.

