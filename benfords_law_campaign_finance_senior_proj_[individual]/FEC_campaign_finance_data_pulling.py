import os
import requests
import pprint #for debugging os.environ.get

print(os.getcwd())
env_var = os.environ # Get the list of user's environment variables
print("User's Environment variable:")
pprint.pprint(dict(env_var), width = 1)
PRIVATE_API_KEY = os.environ.get("PRIVATE_API_KEY")
# The above line works in the Python Console but not when the script is run. It's really weird, like os.environ.get works in Python Console but not terminal?
#I'm going to look more into os.environ.get. Maybe the environments aren't the same in the Consol and the Terminal, like this stackoverflow suggests:
#https://stackoverflow.com/questions/50169910/print-to-file-works-from-console-but-not-when-running-script
parameters = {"api_key":PRIVATE_API_KEY, "designation":'J'} #I'll probably need to change this designation
response = requests.get('https://api.open.fec.gov/v1/committees', params = parameters)
print(response.json())
