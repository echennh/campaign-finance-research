- For 2020 election cycle, the election cycle dates were [11-7-2018, 11-3-2020], where brackets indicate inclusive
- 2018 election cycle: [11-9-2016, 11-6-2018]
- 2016 election cycle: [11-5-2014, 11-8-2016]
- 2014 election cycle: [11-7-2012, 11-4-2014]
- 2012 election cycle: [11-3-2010,11-6-2012]
- 2010 election cycle: [11-5-2008 , 11-2-2010]
- 2008 election cycle: [11-8-2006, 11-4-2008]
- 2006 election cycle: [11-3-2004, 11-7-2006]
- 2004 election cycle: [11-6-2002, 11-2-2004]

<br>

Identify the data files I’ll need to download from the FEC’s bulk data, and download them.
- 2016 election cycle: 2013-2014 itoth and 2015-2016 itoth. I already have the 2015-2016 itoth.
- 2014 election cycle: 2011-2012, 2013-2014
- 2012 election cycle: 2009-2010, 2011-2012
- 2010 election cycle: 2007-2008 , 2009-2010
- 2008 election cycle: 2005-2006, 2007-2008
- 2006 election cycle: 2003-2004, 2005-2006
- 2004 election cycle: 2001-2002, 2003-2004


From the FEC Public Records Office:
"For Election Cycles previous to 2004, it was calendar year-to-date. 2004 was the first election cycle in which it was the day after the election in 2002 thru election day 2004."
